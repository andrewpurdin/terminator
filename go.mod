module terminator

go 1.12

require (
	github.com/aws/aws-sdk-go v1.20.15
	github.com/manifoldco/promptui v0.3.2
)
