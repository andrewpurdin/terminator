package main

import (
	"terminator/src/client"
	"terminator/src/inputs"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/autoscaling"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/manifoldco/promptui"

	"flag"
	"fmt"
	"os"
)

func main() {
	//Declare global variables
	var profile, region, instanceInputResult string
	var err error

	//Take in flags from commandline if passed and parse them
	flag.StringVar(&profile, "profile", "", "profile")
	flag.StringVar(&region, "region", "", "region")
	flag.StringVar(&instanceInputResult, "instance", "", "instance")
	helpPtr := flag.String("help", "", "help")

	if *helpPtr != "" {
		fmt.Printf(`Simply running the application will prompt for user for all inputs needed. Additional flags can be passed for ease of use:

--profile: Accepts the profile input from the command line

--region: Accepts the region from the command line
		
--instance: Accepts the instance from the command line in the format of i-#############`)
		os.Exit(3)
	}

	flag.Parse()

	//Introduction
	fmt.Println(`
                   <((((((\\\
                   /      . }\
                   ;--..--._|}
(\                 '--/\--'  )
 \\                | '-'  :'|
  \\               . -==- .-|
   \\               \.__.'   \--._
   [\\          __.--|       //  _/'--.
   \ \\       .'-._ ('-----'/ __/      \
    \ \\     /   __>|      | '--.       |
     \ \\   |   \   |     /    /       /
      \ '\ /     \  |     |  _/       /
       \  \       \ |     | /        /
        \  \      \        /`)
	fmt.Printf("\nTerminator is a simple CLI tool designed to easily terminate instances from Autoscaling Groups in AWS. \n\nUse Ctrl-C to cancel at any time. \n\nWith great power comes great responsibility, USE WITH CAUTION!\n\n\n\n\n")

	//Accept user inputs if flags not set
	if profile == "" {
		profile, err = inputs.ProfileInput(os.Stdin)
		if err != nil {
			fmt.Printf("Error when attempting to read string %v: %v\n", profile, err)
			os.Exit(3)
		}
	}

	if region == "" {
		region, err = inputs.RegionInput(os.Stdin)
		if err != nil {
			fmt.Printf("Error when attempting to read string %v: %v\n", region, err)
			os.Exit(3)
		}
	}

	// Create sessions
	sess := client.Sesh(region, profile)

	asSvc := autoscaling.New(sess)
	ec2Svc := ec2.New(sess)

	// Template to colorize user prompts
	colorTemplate := &promptui.SelectTemplates{
		Label:    "{{ . }}",
		Active:   "{{ . | underline | cyan }}",
		Inactive: "{{ . | faint | white }}",
		Selected: "\U00002713 {{ . | green }}",
	}

	// Indefinite loop that does the instance termination work
	for {
		if instanceInputResult == "" {
			//Describe Autoscaling Groups in the target account
			result, err := asSvc.DescribeAutoScalingGroups(nil)
			if err != nil {
				fmt.Printf("Error when attempting to describe autoscaling groups: %v", err)
			}

			//Declare slice to hold Autoscaling groups
			var asgs []string

			//Range over all the autoscaling groups returned in result and append them to the slice
			for _, v := range result.AutoScalingGroups[:] {
				asgs = append(asgs, *v.AutoScalingGroupName)
			}
			asgs = append(asgs, "Exit")

			//Generate a user selection prompt that lists all the Autoscaling Groups
			asgPrompt := promptui.Select{
				Label:     "Select Autoscaling Group to terminate instances from",
				Items:     asgs,
				Templates: colorTemplate,
			}

			_, inputResult, err := asgPrompt.Run()

			if err != nil {
				fmt.Printf("Prompt failed when selecting ASG %v\n", err)
				return
			}
			if inputResult == "Exit" {
				os.Exit(3)
			}

			// Initialize arrays that will be appended to
			var instances []*string
			var instancesPrompt []string

			// Get the Index in the slice of Autoscaling groups and return some stats about those instances
			for i, v := range asgs {
				if v == inputResult {
					for _, v := range result.AutoScalingGroups[i].Instances[:] {
						instances = append(instances, v.InstanceId)
						instancesPrompt = append(instancesPrompt, *v.InstanceId)
					}
				}
			}

			// Present details about the instances
			describeEc2InstancesResult, err := ec2Svc.DescribeInstances(
				&ec2.DescribeInstancesInput{
					InstanceIds: instances,
				},
			)
			describeASGInstancesresult, err := asSvc.DescribeAutoScalingInstances(
				&autoscaling.DescribeAutoScalingInstancesInput{
					InstanceIds: instances,
				},
			)
			fmt.Println("Autoscaling Group Instance Statistics:")
			for i := range instances {
				asgInstance := describeASGInstancesresult.AutoScalingInstances[i]
				ec2instance := describeEc2InstancesResult.Reservations[i].Instances[0]
				fmt.Printf("%v\t\t%v\t\t%v\tLaunched %v\n", *ec2instance.InstanceId, *asgInstance.HealthStatus, *asgInstance.LifecycleState, *ec2instance.LaunchTime)
			}

			// Append options to the user prompt
			instancesPrompt = append(instancesPrompt, "Go Back")
			instancesPrompt = append(instancesPrompt, "Exit")

			// Prompt user to select instance for termination
			instancePrompt := promptui.Select{
				Label:     "Select Instance to terminate",
				Items:     instancesPrompt,
				Templates: colorTemplate,
			}

			_, instanceInputResult, err = instancePrompt.Run()
			if err != nil {
				fmt.Printf("Prompt failed when selecting Instance for termination %v\n", err)
				return
			}

			// Start loop over if user selects Go Back
			if instanceInputResult == "Go Back" {
				instanceInputResult = ""
				continue
			}

			if instanceInputResult == "Exit" {
				os.Exit(3)
			}

			// Prompt user a second time to prevent accidents
			areYouSurePrompt := promptui.Select{
				Label:     "Are you sure you want to do this? This action cannot be undone and the instance will be terminated immediately",
				Items:     []string{"Yes", "No"},
				Templates: colorTemplate,
			}

			_, areYouSureResult, err := areYouSurePrompt.Run()
			if err != nil {
				fmt.Printf("Prompt failed when selecting Instance for termination %v\n", err)
				return
			}

			// Determine if the user bailed out or not
			switch {
			case areYouSureResult != "Yes":
				fmt.Println("User cancelled termination")
				os.Exit(3)
			default:
			}
		}

		// Initialize variable that accepts InstanceID to terminate
		termInput := &autoscaling.TerminateInstanceInAutoScalingGroupInput{
			InstanceId:                     aws.String(instanceInputResult),
			ShouldDecrementDesiredCapacity: aws.Bool(false),
		}
		// Terminate the instance and print the result of the API call
		fmt.Println("Termination Result:")
		termResult, err := asSvc.TerminateInstanceInAutoScalingGroup(termInput)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case autoscaling.ErrCodeScalingActivityInProgressFault:
					fmt.Println(autoscaling.ErrCodeScalingActivityInProgressFault, aerr.Error())
				case autoscaling.ErrCodeResourceContentionFault:
					fmt.Println(autoscaling.ErrCodeResourceContentionFault, aerr.Error())
				default:
					fmt.Println(aerr.Error())
				}
			} else {
				// Print the error, cast err to awserr.Error to get the Code and
				// Message from an error.
				fmt.Println(err.Error())
			}
			return
		}

		fmt.Println(termResult)

		//Target instance for lifecycle tracking
		instance := &autoscaling.DescribeAutoScalingInstancesInput{
			InstanceIds: []*string{
				aws.String(instanceInputResult),
			},
		}

		// Initial check of instance termination state
		status, err := asSvc.DescribeAutoScalingInstances(instance)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case autoscaling.ErrCodeInvalidNextToken:
					fmt.Println(autoscaling.ErrCodeInvalidNextToken, aerr.Error())
				case autoscaling.ErrCodeResourceContentionFault:
					fmt.Println(autoscaling.ErrCodeResourceContentionFault, aerr.Error())
				default:
					fmt.Println(aerr.Error())
				}
			} else {
				// Print the error, cast err to awserr.Error to get the Code and
				// Message from an error.
				fmt.Println(err.Error())
			}
			return
		}

		// Checks instance status every 30 seconds until it is no longer "Terminating"
		for len(status.AutoScalingInstances) > 0 {
			status, err = asSvc.DescribeAutoScalingInstances(instance)
			if err != nil {
				if aerr, ok := err.(awserr.Error); ok {
					switch aerr.Code() {
					case autoscaling.ErrCodeInvalidNextToken:
						fmt.Println(autoscaling.ErrCodeInvalidNextToken, aerr.Error())
					case autoscaling.ErrCodeResourceContentionFault:
						fmt.Println(autoscaling.ErrCodeResourceContentionFault, aerr.Error())
					default:
						fmt.Println(aerr.Error())
					}
				} else {
					// Print the error, cast err to awserr.Error to get the Code and
					// Message from an error.
					fmt.Println(err.Error())
				}
				return
			}
			fmt.Print("\nTerminating")
			//print ..... to show activity
			for i := 0; i <= 3; i++ {
				fmt.Print(".")
				time.Sleep(5 * time.Second)
			}
		}
		// Report success
		fmt.Println("\nInstance Terminated Successfully")

		// Ask user if they would like to start again or exit
		againPrompt := promptui.Select{
			Label:     "Would you like to terminate another instance?",
			Items:     []string{"Yes", "No"},
			Templates: colorTemplate,
		}

		_, againResult, err := againPrompt.Run()
		if err != nil {
			fmt.Printf("Prompt failed when asking to terminate another instance %v\n", err)
			return
		} else if againResult == "No" {
			os.Exit(3)
		}
		instanceInputResult = ""
	}
}

//TODO: Write Tests
