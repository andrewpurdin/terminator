package client

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

// Sesh creates a client session with Shared Config State enabled from a profile and region value and returns a pointer to the session that can be referenced to start new specific session types
func Sesh(region string, profile string) *session.Session {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config:            aws.Config{Region: aws.String(region)},
		Profile:           profile,
		SharedConfigState: session.SharedConfigEnable,
	}))
	return sess
}
