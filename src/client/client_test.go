package client

import "testing"

func TestSesh(t *testing.T) {
	region := "us-west-1"
	profile := "test"
	sess := Sesh(region, profile)

	if *sess.Config.Region != region {
		t.Errorf("Expected region %v, got %v instead", region, *sess.Config.Region)
	}
	// Frankly not sure what other tests we can run. I couldn't find a way to validate the profile or the shared config state and I don't feel this test is very useful. I don't want to reference an actual profile and make an API call since not everyone pulling this down may have any particular profile. I'm sure there is a better way to test this through mocking, but I'm not sure yet how to approach it.
}
