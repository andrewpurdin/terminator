package inputs

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strings"
)

// ProfileInput takes input from stdin of an AWS profile and returns the cleaned up value
func ProfileInput(r io.Reader) (profile string, err error) {
	reader := bufio.NewReader(r)
	fmt.Print("Enter the AWS profile to use: ")
	profile, err = reader.ReadString('\n')
	profile = strings.TrimSuffix(profile, "\n")
	return profile, err
}

// RegionInput takes input from stdin of an AWS region and returns the cleaned up value
func RegionInput(r io.Reader) (region string, err error) {
	reader := bufio.NewReader(r)
	fmt.Print("Enter the AWS region to target: ")
	region, err = reader.ReadString('\n')
	region = strings.TrimSuffix(region, "\n")

	// Validate region input one of the proper values
	regex, err := regexp.MatchString(`[a-z]{2}-[a-z]*-[0-9]{1}`, region)
	if regex != true {
		err = errors.New("Invalid AWS Region")
	}
	return region, err
}
