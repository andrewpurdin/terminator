package inputs

import (
	"strings"
	"testing"
)

func TestProfileInput(t *testing.T) {
	input := strings.NewReader("test\n")
	result, err := ProfileInput(input)
	if err != nil {
		t.Errorf("Error thrown: %v", err)
	}
	if result != "test" {
		t.Error("Result did not match expected value")
	}
}

func TestRegionInput(t *testing.T) {
	input := strings.NewReader("us-west-1\n")
	result, err := RegionInput(input)
	if err != nil {
		t.Errorf("Error thrown: %v", err)
	}
	if result != "us-west-1" {
		t.Error("Result did not match expected value")
	}
	// Test regex input error condition
	input = strings.NewReader("test")
	_, err = RegionInput(input)
	if err.Error() != "Invalid AWS Region" {
		t.Errorf("Expected 'Invalid AWS Region', got %v", err)
	}
}
