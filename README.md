# Terminator

  Terminator is a CLI tool designed to simplify termination of instances from Autoscaling Groups in AWS. 

  With great power comes great responsibility, USE WITH CAUTION!

  ## Prerequisites
  - Go is installed. Terminator was built and tested using Go 1.12
  - Access to an AWS account with Autoscaling groups
  - Permissions to list and remove instances from Autoscaling groups
  - AWS profiles configured in the ~/.aws/credentials directory

  ## Getting Started

  After cloning down the repo, you can simply run `make all` to install the dependencies and build the application. Terminator is installed to `/usr/local/bin` by default. 

  Run Terminator from the CLI using `terminator`.

  

  ## Terminating an Instance
  After install, simply run Terminator from the CLI and you will be prompted for a profile and region to query for autoscaling groups. 

  The autoscaling groups will be returned and you may then select one to terminate an instance from. 

  After this, some data about each instance will be listed, and you can select one for termination.

  A final prompt will appear asking if you are sure, and if confirmed you'll see the result of the API call to terminate the instance. 

  ## Supported Flags 
   - `--profile`: Accepts the profile input from the command line
   - `--region`: Accepts the region from the command line
   - `--instance`: Accepts the instance from the command line in the format of `i-#############`
   - `--help`: Prints out basic help info

  ## Built With
  - [promptui](github.com/manifoldco/promptui) - Prompt users for input
  - [AWS SDK for Go](github.com/aws/aws-sdk-go/aws)

  ## Authors
  - Andrew Purdin
