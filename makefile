all: deps build 

deps:
	@go get github.com/aws/aws-sdk-go/aws
	@go get github.com/aws/aws-sdk-go/aws/awserr
	@go get github.com/aws/aws-sdk-go/aws/session
	@go get github.com/aws/aws-sdk-go/service/autoscaling
	@go get github.com/manifoldco/promptui

build:
	@go build -o /usr/local/bin/terminator ./src

